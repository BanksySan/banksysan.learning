﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BanksySan.Learning.ApplicationInsights.Web
{
    using System.Diagnostics;
    using System.Net;
    using Microsoft.ApplicationInsights.Extensibility;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            var ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            Debug.WriteLine($"Endpoint: {TelemetryConfiguration.Active.TelemetryChannel.EndpointAddress}");
            
            TelemetryConfiguration.Active.TelemetryChannel.EndpointAddress =  "https://localhost:3001/Metrics/Post";
        }
    }
}
