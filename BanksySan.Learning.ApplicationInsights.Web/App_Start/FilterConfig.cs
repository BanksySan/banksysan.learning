﻿using System.Web;
using System.Web.Mvc;

namespace BanksySan.Learning.ApplicationInsights.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ErrorHandler.AiHandleErrorAttribute());
        }
    }
}
