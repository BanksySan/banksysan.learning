﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BanksySan.Learning.ApplicationInsights.Web.Controllers
{
    using System.Web.Http.Cors;
    using Newtonsoft.Json;

    [EnableCors(origins: "*", headers: "Origin, X-Requested-With, Content-Name, Content-Type, Accept", methods: "POST")]
    public class MetricsController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage PostTelemetry([FromBody] dynamic body)
        {
            string s = JsonConvert.SerializeObject(body);
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }
    }
}
