﻿namespace BanksySan.Learning.ApplicationInsights.Web.Metrics
{
    using System;
    using System.IO;
    using Microsoft.ApplicationInsights.Channel;
    using Microsoft.ApplicationInsights.DataContracts;
    using Newtonsoft.Json;

    public class FileSystemChannel : ITelemetryChannel
    {
        private static readonly DirectoryInfo BASE_DIRECTORY = new DirectoryInfo(@"...\TelemetryDump");

        private bool? _developerMode;

        /// <inheritdoc />
        static FileSystemChannel()
        {
            if (!BASE_DIRECTORY.Exists)
            {
                BASE_DIRECTORY.Create();
            }
        }

        public void Dispose()
        {
        }

        public void Send(ITelemetry item)
        {
            var saneItem = new SaneTelemetry
            {
                TimeStamp = item.Timestamp,
                Type = item.GetType().Name
            };

            saneItem.Details = GetDetails(item);

            var fileName = BASE_DIRECTORY.FullName + @"\" + DateTime.Now.Ticks;
            using (var writer = File.CreateText(fileName))
            {
                writer.Write(JsonConvert.SerializeObject(saneItem));
            }
        }

        public void Flush()
        {
        }

        public bool? DeveloperMode { get; set; }

        public string EndpointAddress { get; set; }

        private dynamic GetDetails(ITelemetry item)
        {
            if (item is ExceptionTelemetry)
            {
                return GetDetails((ExceptionTelemetry) item);
            }

            if (item is RequestTelemetry)
            {
                return GetDetails((RequestTelemetry) item);
            }

            // Implement the rest as you see fit.

            return null;
        }


        private dynamic GetDetails(RequestTelemetry requestTelemetry)
        {
            return new
            {
                requestTelemetry.Duration,
                requestTelemetry.Name,
                requestTelemetry.Url
            };
        }

        private dynamic GetDetails(ExceptionTelemetry exceptionTelemetry)
        {
            return new
            {
                exceptionTelemetry.Exception.Message,
                exceptionTelemetry.Exception.GetType().Name
            };
        }
    }

    public class SaneTelemetry
    {
        public string Type { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public dynamic Details { get; set; }
    }
}