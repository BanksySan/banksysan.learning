﻿namespace BanksySan.Learning.ClrMD.Data
{
    using System;

    internal interface ISegmentInfo
    {
        Guid ProbeId { get; }
        ulong Start { get; }
        ulong CommitedEnd { get; }
        ulong ReservedEnd { get; }
        ulong Length { get; }
        bool Ephemeral { get; }
        bool Large { get; }
        int Index { get; }
    }
}