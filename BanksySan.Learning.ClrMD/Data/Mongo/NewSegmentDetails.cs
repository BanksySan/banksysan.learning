﻿namespace BanksySan.Learning.ClrMD.Data.Mongo
{
    using System;
    using MongoDB.Bson;
    using MongoDB.Driver;

    internal class NewSegmentDetails : INewSegmentDetails
    {
        private readonly IMongoCollection<BsonDocument> _collection;

        public NewSegmentDetails(IMongoCollection<BsonDocument> collection)
        {
            _collection = collection;
        }

        /// <inheritdoc />
        public void Execute(ISegmentInfo segmentInfo)
        {
            var document = new BsonDocument
            {
                {"type", "segment"},
                {"probeId", segmentInfo.ProbeId},
                {"index", segmentInfo.Index},
                {"startAddress", BitConverter.GetBytes(segmentInfo.Start)},
                {"endCommited", BitConverter.GetBytes(segmentInfo.CommitedEnd)},
                {"endReserved", BitConverter.GetBytes(segmentInfo.ReservedEnd)},
                {"length", BitConverter.GetBytes(segmentInfo.Length)},
                {"ephemeral", segmentInfo.Ephemeral},
                {"large", segmentInfo.Large}
            };

            _collection.InsertOneAsync(document);
        }
    }
}