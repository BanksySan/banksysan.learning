﻿namespace BanksySan.Learning.ClrMD.Data.Mongo
{
    using System;
    using MongoDB.Bson;
    using MongoDB.Driver;

    internal class NewGenerationDetails : INewGenerationDetails
    {
        private readonly IMongoCollection<BsonDocument> _collection;

        /// <inheritdoc />
        public NewGenerationDetails(IMongoCollection<BsonDocument> collection)
        {
            _collection = collection;
        }

        /// <inheritdoc />
        public void Execute(IGenerationInfo info)
        {
            var document = new BsonDocument
            {
                {"generation", info.Generation},
                {"probeId", info.ProbeId},
                {"startAddress", BitConverter.GetBytes(info.StartAddress)},
                {"segmentIndex", info.SegmentIndex},
                {"type", "generation" }
            };

            _collection.InsertOne(document);
        }
    }
}