﻿namespace BanksySan.Learning.ClrMD.Data.Mongo
{
    using MongoDB.Bson;
    using MongoDB.Driver;

    internal class NewProbe : INewProbe
    {
        private readonly IMongoCollection<BsonDocument> _collection;

        public NewProbe(IMongoCollection<BsonDocument> collection)
        {
            _collection = collection;
        }

        /// <inheritdoc />
        public void Execute(IProbeInfo probeInfo)
        {
            var document = new BsonDocument
            {
                {"_id", probeInfo.Id},
                {"type", "probe"},
                {"runId", probeInfo.RunId},
                {"recordedTime", probeInfo.RecordedTime}
            };

            _collection.InsertOne(document);
        }
    }
}