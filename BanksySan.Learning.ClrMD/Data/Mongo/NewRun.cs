﻿namespace BanksySan.Learning.ClrMD.Data.Mongo
{
    using Application;
    using MongoDB.Bson;
    using MongoDB.Driver;

    internal class NewRun : INewRun
    {
        private readonly IMongoCollection<BsonDocument> _collection;

        public NewRun(IMongoCollection<BsonDocument> collection)
        {
            _collection = collection;
        }

        /// <inheritdoc />
        public void Execute(IRunInfo runInfo)
        {
            var document = new BsonDocument
            {
                {"id", runInfo.Id},
                {"type", "run"},
                {"time", runInfo.RecordedTime}
            };

            _collection.InsertOneAsync(document);
        }
    }
}