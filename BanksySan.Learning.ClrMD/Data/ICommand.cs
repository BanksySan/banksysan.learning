﻿namespace BanksySan.Learning.ClrMD.Data
{
    public interface ICommand<in T>
    {
        void Execute(T info);
    }
}