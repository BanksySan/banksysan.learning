﻿namespace BanksySan.Learning.ClrMD.Data.Sql
{
    using System.Data;

    internal class NewGenerationDetails : INewGenerationDetails
    {
        private readonly IDbConnection _connection;

        public NewGenerationDetails(IDbConnection connection)
        {
            _connection = connection;
        }

        public void Execute(IGenerationInfo info)
        {
            using (var command = _connection.CreateCommand())
            {
                command.CommandText = "[AddGenerationData]";
                command.CommandType = CommandType.StoredProcedure;

                var probeIdParameter = command.CreateParameter();
                probeIdParameter.Value = info.ProbeId;
                probeIdParameter.DbType = DbType.Guid;
                probeIdParameter.ParameterName = "@ProbeId";

                var generationId = command.CreateParameter();
                generationId.DbType = DbType.Byte;
                generationId.ParameterName = "@GenerationId";
                generationId.Value = info.Generation;

                var startAddress = command.CreateParameter();
                startAddress.DbType = DbType.Binary;
                startAddress.Size = 8;
                startAddress.ParameterName = "@StartAddress";
                startAddress.Value = info.StartAddress;

                var size = command.CreateParameter();
                size.DbType = DbType.Binary;
                size.Size = 8;
                size.ParameterName = "@Size";
                size.Value = info.Length;

                command.Parameters.Add(probeIdParameter);
                command.Parameters.Add(startAddress);
                command.Parameters.Add(generationId);
                command.Parameters.Add(size);
                command.ExecuteNonQuery();
            }
        }
    }
}