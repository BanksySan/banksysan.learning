namespace BanksySan.Learning.ClrMD.Data.Sql
{
    using System.Data;
    using Application;

    internal class NewRun : INewRun
    {
        private readonly IDbConnection _connection;

        public NewRun(IDbConnection connection)
        {
            _connection = connection;
        }

        /// <inheritdoc />
        public void Execute(IRunInfo runInfo)
        {
            using (var newRunCommand = _connection.CreateCommand())
            {
                newRunCommand.CommandType = CommandType.StoredProcedure;
                newRunCommand.CommandText = "AddRun";
                newRunCommand.Connection = _connection;

                var newRunTimeParameter = newRunCommand.CreateParameter();
                newRunTimeParameter.DbType = DbType.DateTime;
                newRunTimeParameter.ParameterName = "@Time";
                newRunTimeParameter.Value = runInfo.RecordedTime;

                var applicationNameParameter = newRunCommand.CreateParameter();
                applicationNameParameter.DbType = DbType.String;
                applicationNameParameter.ParameterName = "@ApplicationName";
                applicationNameParameter.Value = "Test";

                var newIdParameter = newRunCommand.CreateParameter();
                newIdParameter.ParameterName = "@NewID";
                newIdParameter.DbType = DbType.Guid;
                newIdParameter.Value = runInfo.Id;

                newRunCommand.Parameters.Add(newRunTimeParameter);
                newRunCommand.Parameters.Add(applicationNameParameter);
                newRunCommand.Parameters.Add(newIdParameter);

                newRunCommand.ExecuteNonQuery();
            }
        }
    }
}