﻿namespace BanksySan.Learning.ClrMD.Data.Sql
{
    using System.Data;

    internal class NewProbe : INewProbe
    {
        private readonly IDbConnection _connection;

        /// <inheritdoc />
        public NewProbe(IDbConnection connection)
        {
            _connection = connection;
        }

        /// <inheritdoc />
        public void Execute(IProbeInfo probeInfo)
        {
            using (var probeCommand = _connection.CreateCommand())
            {
                probeCommand.CommandText = "AddProbe";
                probeCommand.CommandType = CommandType.StoredProcedure;
                probeCommand.Connection = _connection;

                var probeIdParameter = probeCommand.CreateParameter();
                probeIdParameter.ParameterName = "@ProbeId";
                probeIdParameter.DbType = DbType.Guid;
                probeIdParameter.Direction = ParameterDirection.Output;

                var runIdParameter = probeCommand.CreateParameter();
                runIdParameter.Value = probeInfo.Id;
                runIdParameter.DbType = DbType.Guid;
                runIdParameter.ParameterName = "@RunId";

                probeCommand.Parameters.Add(runIdParameter);
                probeCommand.Parameters.Add(probeIdParameter);

                probeCommand.ExecuteNonQuery();
            }
        }
    }
}