﻿namespace BanksySan.Learning.ClrMD.Data.Sql
{
    using System;
    using System.Data;

    internal class NewSegmentDetails : INewSegmentDetails
    {
        private readonly IDbConnection _connection;

        /// <inheritdoc />
        public NewSegmentDetails(IDbConnection connection)
        {
            _connection = connection;
        }


        /// <inheritdoc />
        public void Execute(ISegmentInfo info)
        {
            using (var segmentDetailsCommand = _connection.CreateCommand())
            {
                segmentDetailsCommand.CommandText = "[AddSegmentData]";
                segmentDetailsCommand.CommandType = CommandType.StoredProcedure;
                segmentDetailsCommand.Connection = _connection;

                var probeIdParameter = segmentDetailsCommand.CreateParameter();
                probeIdParameter.DbType = DbType.Guid;
                probeIdParameter.ParameterName = "@ProbeId";
                probeIdParameter.Value = info.ProbeId;

                var segmentStart = segmentDetailsCommand.CreateParameter();
                segmentStart.DbType = DbType.Binary;
                segmentStart.Size = 8;
                segmentStart.ParameterName = "@StartAddress";

                var segmentSize = segmentDetailsCommand.CreateParameter();
                segmentSize.DbType = DbType.Binary;
                segmentSize.Size = 8;
                segmentSize.ParameterName = "@Size";

                var commitedEnd = segmentDetailsCommand.CreateParameter();
                commitedEnd.DbType = DbType.Binary;
                commitedEnd.Size = 8;
                commitedEnd.ParameterName = "@CommitedEnd";

                var isLarge = segmentDetailsCommand.CreateParameter();
                isLarge.DbType = DbType.Boolean;
                isLarge.ParameterName = "@IsLarge";

                var isEphemeral = segmentDetailsCommand.CreateParameter();
                isEphemeral.DbType = DbType.Boolean;
                isEphemeral.ParameterName = "@IsEphemeral";

                var reservedEnd = segmentDetailsCommand.CreateParameter();
                reservedEnd.DbType = DbType.Binary;
                reservedEnd.Size = 8;
                reservedEnd.ParameterName = "@ReservedEnd";

                segmentDetailsCommand.Parameters.Add(probeIdParameter);
                segmentDetailsCommand.Parameters.Add(segmentStart);
                segmentDetailsCommand.Parameters.Add(segmentSize);
                segmentDetailsCommand.Parameters.Add(isLarge);
                segmentDetailsCommand.Parameters.Add(isEphemeral);
                segmentDetailsCommand.Parameters.Add(commitedEnd);
                segmentDetailsCommand.Parameters.Add(reservedEnd);

                probeIdParameter.Value = info.ProbeId;
                commitedEnd.Value = BitConverter.GetBytes(info.CommitedEnd);
                segmentStart.Value = BitConverter.GetBytes(info.Start);
                segmentSize.Value = BitConverter.GetBytes(info.Length);
                isEphemeral.Value = info.Ephemeral;
                isLarge.Value = info.Length;
                reservedEnd.Value = BitConverter.GetBytes(info.ReservedEnd);

                segmentDetailsCommand.ExecuteNonQuery();
            }
        }
    }
}