﻿namespace BanksySan.Learning.ClrMD.Data
{
    using Application;

    internal interface INewRun : ICommand<IRunInfo>
    {
    }
}