﻿namespace BanksySan.Learning.ClrMD.Data
{
    using System;

    internal interface IProbeInfo
    {
        Guid Id { get; }
        Guid RunId { get; }
        DateTime RecordedTime { get; }
    }
}