﻿namespace BanksySan.Learning.ClrMD.Data
{
    using System;

    internal interface IGenerationInfo
    {
        Guid ProbeId { get; }
        int SegmentIndex { get; }
        byte Generation { get; }
        ulong StartAddress { get; }
        ulong Length { get; }
    }
}