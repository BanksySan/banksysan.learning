﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;

    internal class RunInfo : IRunInfo
    {
        public RunInfo(DateTime recordedTime, Guid id)
        {
            RecordedTime = recordedTime;
            Id = id;
        }

        /// <inheritdoc />
        public Guid Id { get; }

        /// <inheritdoc />
        public DateTime RecordedTime { get; }
    }
}