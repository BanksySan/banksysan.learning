namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Data;
    using Microsoft.Diagnostics.Runtime;

    internal class Application : IApplication
    {
        private readonly CancellationToken _cancellationToken;
        private readonly Action _disposeAction;
        private readonly INewGenerationDetails _generationDetails;
        private readonly INewProbe _newProbe;
        private readonly INewRun _newRun;
        private readonly INewSegmentDetails _newSegmentDetails;
        private readonly Process _process;

        public Application(IApplicationArgs applicationArgs, CancellationToken cancellationToken,
                           Action disposeAction)
        {
            _disposeAction = disposeAction;
            _cancellationToken = cancellationToken;
            _newProbe = applicationArgs.NewProbeCommand;
            _newRun = applicationArgs.NewRunCommand;
            _generationDetails = applicationArgs.NewGenerationDetails;
            _newSegmentDetails = applicationArgs.NewSegmentDetails;
            _process = applicationArgs.TargetProcess;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _disposeAction?.Invoke();
        }

        /// <inheritdoc />
        public Task Execute()
        {
            var task = new Task(StartRun, _cancellationToken);
            task.Start();
            return task;
        }

        private void StartRun()
        {
            var runId = Guid.NewGuid();
            _newRun.Execute(new RunInfo(DateTime.Now, runId));


            while (true)
            {
                var probeId = Guid.NewGuid();
                _newProbe.Execute(new ProbeInfo(probeId, runId, DateTime.Now));

                using (var target = DataTarget.AttachToProcess(_process.Id, 2000))
                {
                    var clr = target.ClrVersions.Single();
                    var runtime = clr.CreateRuntime();
                    var heap = runtime.GetHeap();
                    Console.WriteLine("New probe: {0}", probeId);

                    for (var segmentIndex = 0; segmentIndex < heap.Segments.Count; segmentIndex++)
                    {
                        

                        var segment = heap.Segments[segmentIndex];
                        const byte GENERATION_0_ID = 0;
                        var generation0Start = segment.Gen0Start;
                        var generation0Length = segment.Gen0Length;

                        const byte GENERATION_1_ID = 1;
                        var generation1Start = segment.Gen1Start;
                        var generation1Length = segment.Gen1Length;

                        const byte GENERATION_2_ID = 2;
                        var generation2Start = segment.Gen2Start;
                        var generation2Length = segment.Gen2Length;

                        _newSegmentDetails.Execute(new SegmentInfo(probeId, segmentIndex, segment.Start,
                                                                   segment.CommittedEnd,
                                                                   segment.ReservedEnd, segment.Length,
                                                                   segment.IsEphemeral, segment.IsLarge));

                        _generationDetails.Execute(new GenerationInfo(probeId, segmentIndex, GENERATION_0_ID,
                                                                      generation0Start,
                                                                      generation0Length));
                        _generationDetails.Execute(new GenerationInfo(probeId, segmentIndex, GENERATION_1_ID,
                                                                      generation1Start,
                                                                      generation1Length));
                        _generationDetails.Execute(new GenerationInfo(probeId, segmentIndex, GENERATION_2_ID,
                                                                      generation2Start,
                                                                      generation2Length));
                        Console.WriteLine($"Segment {segmentIndex}: {segment.Start:X} Ephemeral: {segment.IsEphemeral}");
                    }
                    Console.WriteLine();
                }

                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }
    }
}