﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using Data;

    internal class GenerationInfo : IGenerationInfo
    {
        /// <inheritdoc />
        public GenerationInfo(Guid probeId, int segmentIndex, byte generation, ulong startAddress, ulong length)
        {
            ProbeId = probeId;
            SegmentIndex = segmentIndex;
            Generation = generation;
            StartAddress = startAddress;
            Length = length;
        }

        public ulong Length { get; }

        /// <inheritdoc />
        public Guid ProbeId { get; }

        public int SegmentIndex { get; }

        /// <inheritdoc />
        public byte Generation { get; }

        /// <inheritdoc />
        public ulong StartAddress { get; }
    }
}