﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using System.Threading.Tasks;

    internal interface IApplication : IDisposable
    {
        Task Execute();
    }
}