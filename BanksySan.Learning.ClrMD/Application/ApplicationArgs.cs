﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using System.Diagnostics;
    using Data;

    internal class ApplicationArgs : IApplicationArgs
    {
        /// <inheritdoc />
        public ApplicationArgs(Process targetProcess, INewRun newRunCommand, INewProbe newProbeCommand,
                               INewSegmentDetails newSegmentDetails, INewGenerationDetails newGenerationDetails,
                               Action disposeAction)
        {
            TargetProcess = targetProcess;
            NewRunCommand = newRunCommand;
            NewProbeCommand = newProbeCommand;
            NewSegmentDetails = newSegmentDetails;
            NewGenerationDetails = newGenerationDetails;
            DisposeAction = disposeAction;
        }

        /// <inheritdoc />
        public Process TargetProcess { get; }

        /// <inheritdoc />
        public INewRun NewRunCommand { get; }

        /// <inheritdoc />
        public INewProbe NewProbeCommand { get; }

        /// <inheritdoc />
        public INewSegmentDetails NewSegmentDetails { get; }

        /// <inheritdoc />
        public INewGenerationDetails NewGenerationDetails { get; }

        /// <inheritdoc />
        public Action DisposeAction { get; }
    }
}