namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using Data;

    internal class SegmentInfo : ISegmentInfo
    {
        /// <inheritdoc />
        public SegmentInfo(Guid probeId, int index, ulong start, ulong commitedEnd, ulong reservedEnd, ulong length,
                           bool ephemeral, bool large)
        {
            ProbeId = probeId;
            Index = index;
            Start = start;
            CommitedEnd = commitedEnd;
            ReservedEnd = reservedEnd;
            Length = length;
            Ephemeral = ephemeral;
            Large = large;
        }

        /// <inheritdoc />
        public Guid ProbeId { get; }

        /// <inheritdoc />
        public ulong Start { get; }

        /// <inheritdoc />
        public ulong CommitedEnd { get; }

        /// <inheritdoc />
        public ulong ReservedEnd { get; }

        /// <inheritdoc />
        public ulong Length { get; }

        /// <inheritdoc />
        public bool Ephemeral { get; }

        /// <inheritdoc />
        public bool Large { get; }

        /// <inheritdoc />
        public int Index { get; }
    }
}