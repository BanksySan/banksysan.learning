namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using Data;

    internal class ProbeInfo : IProbeInfo
    {
        /// <inheritdoc />
        public ProbeInfo(Guid id, Guid runId, DateTime recordedTime)
        {
            Id = id;
            RunId = runId;
            RecordedTime = recordedTime;
        }

        /// <inheritdoc />
        public Guid Id { get; }

        /// <inheritdoc />
        public Guid RunId { get; }

        /// <inheritdoc />
        public DateTime RecordedTime { get; }
    }
}