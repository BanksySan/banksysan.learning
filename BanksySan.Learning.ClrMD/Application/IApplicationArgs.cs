﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;
    using System.Diagnostics;
    using Data;

    internal interface IApplicationArgs
    {
        Process TargetProcess { get; }
        INewRun NewRunCommand { get; }
        INewProbe NewProbeCommand { get; }
        INewSegmentDetails NewSegmentDetails { get; }
        INewGenerationDetails NewGenerationDetails { get; }
        Action DisposeAction { get; }
    }
}