﻿namespace BanksySan.Learning.ClrMD.Application
{
    using System;

    internal interface IRunInfo
    {
        Guid Id { get; }
        DateTime RecordedTime { get; }
    }
}