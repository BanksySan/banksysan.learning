﻿namespace BanksySan.Learning.ClrMD
{
    using System;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Threading;
    using Application;
    using Data;
    using Data.Mongo;
    using MongoDB.Bson;
    using MongoDB.Driver;
    using static System.Console;

    internal static class Program
    {
        private static CancellationToken _CANCELLATION_TOKEN;

        private static void Exit(object sender, ConsoleCancelEventArgs e)
        {
            WriteLine("Cancelling...");
            _CANCELLATION_TOKEN = new CancellationToken(false);
        }

        private static int Main(string[] args)
        {
            CancelKeyPress += Exit;
            WindowWidth = 50;
            BufferWidth = WindowWidth;
            WindowHeight = 50;

            const string FILE_NAME =
                @"..\..\..\BanksySan.Learning.ClrMD.Target\bin\Release\BanksySan.Learning.ClrMD.Target.exe";

            var process =
                Process
                    .Start(FILE_NAME);
            if (process == null)
            {
                throw new Exception($"Could not start process: {FILE_NAME}");
            }

            INewRun newRun;
            INewProbe newProbe;
            INewSegmentDetails newSegmentDetails;
            INewGenerationDetails newGenerationDetails;
            Action disposeAction = null;

            switch (args[0])
            {
                case "mongo":
                    var mongoClient = new MongoClient();
                    var mongoDatabase = mongoClient.GetDatabase("ClrMD");
                    var mongoCollection = mongoDatabase.GetCollection<BsonDocument>("Default");
                    newRun = new NewRun(mongoCollection);
                    newProbe = new NewProbe(mongoCollection);
                    newSegmentDetails = new NewSegmentDetails(mongoCollection);
                    newGenerationDetails = new NewGenerationDetails(mongoCollection);
                    break;

                case "sql":

                    var connection =
                        new SqlConnection("Data Source=localhost;Initial Catalog=Telemetry;Integrated Security=True");
                    connection.Open();
                    newRun = new Data.Sql.NewRun(connection);
                    newProbe = new Data.Sql.NewProbe(connection);
                    newSegmentDetails = new Data.Sql.NewSegmentDetails(connection);
                    newGenerationDetails = new Data.Sql.NewGenerationDetails(connection);
                    disposeAction = () => connection?.Dispose();
                    break;

                default:
                    WriteLine("You must specify a datastore mechanism.");
                    return 1;
            }

            var applicationArgs = new ApplicationArgs(process, newRun, newProbe, newSegmentDetails,
                                                      newGenerationDetails, disposeAction);

            using (var application = new Application.Application(applicationArgs, _CANCELLATION_TOKEN, disposeAction))
            {
                var task = application.Execute();
                task.Wait();
            }

            return 0;
        }
    }
}