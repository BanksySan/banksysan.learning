﻿namespace BanksySan.Learning.ApplicationInsights.Tests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.ApplicationInsights;
    using Microsoft.ApplicationInsights.DataContracts;
    using NUnit.Framework;

    [TestFixture]
    public class SimpleCallTests
    {
        [TearDown]
        public void TearDown()
        {
            _CLIENT.Flush();
        }

        [OneTimeSetUp]
        public static void OneTimeSetUp()
        {
            _CLIENT = new TelemetryClient {InstrumentationKey = Configuration.INSTRUMENTATION_KEY};
            _CLIENT.Context.Properties["TestName"] = nameof(SimpleCallTests);
            _CLIENT.Context.Operation.CorrelationVector = Guid.NewGuid().ToString("B");
        }

        private static TelemetryClient _CLIENT;

        [Test]
        public void FireAggregateException()
        {
            var subAggregate = new AggregateException("Sub-aggregate exception", new Exception("Exception 1a"),
                                                      new Exception("Exception 2a", new Exception("Exception 2b")));

            var aggregate = new AggregateException("Parent", subAggregate);

            _CLIENT.TrackException(aggregate);
        }

        [Test]
        public void FireAvailability()
        {
            const bool SUCCESSFUL = true;

            _CLIENT.TrackAvailability("Ping!", DateTimeOffset.Now, TimeSpan.FromSeconds(5), "My computer", SUCCESSFUL,
                                     "Yey!  The application is there.");
        }

        [Test]
        public void FireDefaultMetric()
        {
            var metric = Math.Sin(DateTime.Now.Ticks);

            _CLIENT.TrackMetric("Sin of the Times", metric);
        }

        [Test]
        public void FireDependency()
        {
            const bool SUCCESSFUL = true;
            _CLIENT.TrackDependency("My dependency", "My target", DateTimeOffset.Now, TimeSpan.FromSeconds(42),
                                   SUCCESSFUL);
        }

        [Test]
        public void FireEvent()
        {
            _CLIENT.TrackEvent(new EventTelemetry("The Event() test has been run."));

            _CLIENT.Flush();
        }

        [Test]
        public void FireException()
        {
            _CLIENT.TrackException(new Exception("Exception 1a"));
        }

        [Test]
        public void FireMetricsWithProperties()
        {
            _CLIENT.TrackMetric("Metrics with dictionary", 123.123d, new Dictionary<string, string>
            {
                {"One", "1"},
                {"Two", "2"},
                {"Three", "3"},
                {"Four", "4"},
                {"Five", "5"},
                {"Six", "6"},
                {"Seven", "7"}
            });
        }

        [Test]
        public void FirePageView()
        {
            var pageViewTelemetry = new PageViewTelemetry
            {
                Url = new Uri("https://example.com/Telemetry"),
                Duration = TimeSpan.FromMinutes(10)
            };

            _CLIENT.TrackPageView(pageViewTelemetry);
        }

        [Test]
        public void FireTelemetry()
        {
            const bool SUCCESS = true;
            var r = new RequestTelemetry("HTTP request", DateTimeOffset.Now, TimeSpan.FromSeconds(10),
                                         "418 I'm a teapot", SUCCESS) {Id = "Hello Dave"};

            _CLIENT.TrackRequest(r);
        }

        [Test]
        public void FireTrace()
        {
            _CLIENT.TrackTrace("This is a trace.", SeverityLevel.Critical);
        }
    }
}