﻿namespace BanksySan.Learning.ApplicationInsights.Tests
{
    using Microsoft.ApplicationInsights;
    using Microsoft.ApplicationInsights.Extensibility;
    using NUnit.Framework;
    using Stub;
    using static System.Console;
    using static System.Threading.Thread;

    [TestFixture]
    public class ConfigurationTests
    {
        [Test]
        public void ChangeHost()
        {
            var telemetryConfiguration = new TelemetryConfiguration
            {
                TelemetryChannel = new FileSystemChannel(),
                InstrumentationKey = Configuration.INSTRUMENTATION_KEY,
                DisableTelemetry = false
            };
            var telemetryClient = new TelemetryClient(telemetryConfiguration);

            try
            {
                for (var i = 0; i < 10; i++)
                {
                    telemetryClient.TrackEvent($"Test: {i}");
                }
            }
            finally
            {
                telemetryClient.Flush();
                Sleep(5000);
                WriteLine("Flushed");
            }
        }
    }
}