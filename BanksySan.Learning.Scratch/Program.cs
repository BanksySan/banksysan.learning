﻿namespace BanksySan.Learning.Scratch
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using static System.Console;

    internal static class Program
    {
        private const string CONNECTION_STRING = @"Data Source=plod;Initial Catalog=Scratch;Integrated Security=True";
        private const string COMMAND_TEXT = @"SELECT [Id],[Description] FROM [dbo].[Table1]; SELECT [Id],[Description] FROM [dbo].[Table2];";

        private static void Main()
        {
            WindowWidth = 100;
            BufferWidth = 500;
            Title = $"Scratch: {Process.GetCurrentProcess().Id}";


            var dataSet = GetData();

        }

        private static DataSet GetData()
        {
            var dataSet = new DataSet();
            using (var connection =
                new SqlConnection(CONNECTION_STRING))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = COMMAND_TEXT;
                    command.CommandType = CommandType.Text;
                    connection.Open();  
                    
                    using (var adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dataSet);
                        return dataSet;
                    }
                }
            }
        }
    }
}