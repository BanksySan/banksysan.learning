﻿namespace BanksySan.Learning.ApplicationInsights.Stub
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using Microsoft.ApplicationInsights.Channel;
    using Microsoft.ApplicationInsights.DataContracts;
    using Newtonsoft.Json;

    public class FileSystemChannel : ITelemetryChannel
    {
        private static readonly JsonSerializerSettings SETTINGS = new JsonSerializerSettings();
        private static readonly DirectoryInfo baseDirectory = new DirectoryInfo(@"C:\temp\TelemetryDump");

        private readonly DirectoryInfo _baseDirectory;
        private bool? _developerMode;

        /// <inheritdoc />
        public FileSystemChannel()
        {
            Debug.WriteLine("FileSystemChannet: Constructor");
            baseDirectory.Refresh();
            if (!baseDirectory.Exists)
            {
                baseDirectory.Create();
            }
            _baseDirectory = baseDirectory;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            Debug.WriteLine("Disposing");
        }

        /// <inheritdoc />
        public void Send(ITelemetry item)
        {
            var saneItem = new SaneTelemetry
            {
                TimeStamp = item.Timestamp,
                Type = item.GetType().Name
            };

            switch (item)
            {
                case RequestTelemetry telemetry:
                    saneItem.Details = GetDetails(telemetry);
                    break;
                case ExceptionTelemetry telemetry:
                    saneItem.Details = GetDetails(telemetry);
                    break;
            }

            using (var writer = File.CreateText(_baseDirectory.FullName + @"\" + DateTime.Now.Ticks))
            {
                var serialised = JsonConvert.SerializeObject(saneItem, Formatting.Indented);
                writer.Write(serialised);
            }
        }

        private dynamic GetDetails(RequestTelemetry requestTelemetry)
        {
            return new
            {
                requestTelemetry.Duration,
                requestTelemetry.Name,
                requestTelemetry.Url
            };
        }

        private dynamic GetDetails(ExceptionTelemetry exceptionTelemetry)
        {
            return new
            {
                exceptionTelemetry.Exception.Message,
                exceptionTelemetry.Exception.GetType().Name
            };
        }

        /// <inheritdoc />
        public void Flush()
        {
            Debug.WriteLine("Flush");
        }

        /// <inheritdoc />
        public bool? DeveloperMode
        {
            get
            {
                Debug.WriteLine("Get DeveloperMode");
                return _developerMode;
            }
            set
            {
                _developerMode = value;
                Debug.WriteLine($"Get DeveloperMode {value}");
            }
        }

        /// <inheritdoc />
        public string EndpointAddress { get; set; }
    }

    public class SaneTelemetry
    {
        public string Type { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        public dynamic Details { get; set; }
    }
}