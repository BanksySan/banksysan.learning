namespace BanksySan.Learning.ClrMD.Web.Models
{
    using System;
    using System.Collections.Generic;

    public class RunInfo
    {
        public RunInfo(IEnumerable<ProbeInfo> probes, DateTime dateTime)
        {
            Probes = probes;
            DateTime = dateTime;
        }

        public IEnumerable<ProbeInfo> Probes { get; }
        public DateTime DateTime { get; }
    }
}