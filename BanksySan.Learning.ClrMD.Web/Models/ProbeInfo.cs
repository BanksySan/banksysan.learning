﻿namespace BanksySan.Learning.ClrMD.Web.Models
{
    using System;
    using System.Collections.Generic;

    public class ProbeInfo
    {
        public ProbeInfo(IEnumerable<SegmentInfo> segments, DateTime time)
        {
            Segments = segments;
            Time = time;
        }

        public IEnumerable<SegmentInfo> Segments { get; }
        public DateTime Time { get; }
    }
}