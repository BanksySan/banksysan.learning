﻿namespace BanksySan.Learning.ClrMD.Web.Models
{
    using System;

    public class RunSummary
    {
        public RunSummary(DateTime dateTime, Guid runId)
        {
            DateTime = dateTime;
            RunId = runId;
        }

        public DateTime DateTime { get; }
        public Guid RunId { get; }
    }
}