﻿namespace BanksySan.Learning.ClrMD.Web.Models
{
    public class SegmentInfo
    {
        /// <inheritdoc />
        public SegmentInfo(ulong startAddress, ulong length, ulong commitedEnd, bool ephemaral, bool large,
                           ulong reservedEnd)
        {
            StartAddress = startAddress;
            Length = length;
            CommitedEnd = commitedEnd;
            Ephemaral = ephemaral;
            Large = large;
            ReservedEnd = reservedEnd;
        }

        public ulong StartAddress { get; }
        public ulong Length { get; }
        public ulong CommitedEnd { get; }
        public bool Ephemaral { get; }
        public bool Large { get; }
        public ulong ReservedEnd { get; }
    }
}