﻿namespace BanksySan.Learning.ClrMD.Web.Controllers
{
    using Data;
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        private static readonly GetRuns GET_RUNS = new GetRuns();

        public IActionResult Index()
        {
            var runs = GET_RUNS.Execute();

            return View(runs);
        }
    }
}