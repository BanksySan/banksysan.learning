namespace BanksySan.Learning.ClrMD.Web.Controllers
{
    using System;
    using Microsoft.AspNetCore.Mvc;
    using Data;

    public class RunsController : Controller
    {
        private static readonly GetRunDetail GET_RUN_DETAIL = new GetRunDetail();

        public IActionResult Index(Guid id)
        {
            var runDetail = GET_RUN_DETAIL.Execute(id);

            return View(runDetail);
        }
    }
}