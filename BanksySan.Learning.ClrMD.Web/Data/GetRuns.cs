﻿namespace BanksySan.Learning.ClrMD.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Models;

    public class GetRuns
    {
        public IEnumerable<RunSummary> Execute()
        {
            using (var connection =
                new SqlConnection("Data Source=localhost;Initial Catalog=Telemetry;Integrated Security=True"))
            {
                connection.Open();
                var command = connection.CreateCommand();
                command.CommandText = "GetRuns";
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = connection;

                var dataReader = command.ExecuteReader();

                var runs = new List<RunSummary>();

                while (dataReader.Read())
                {
                    var runId = (Guid) dataReader["Id"];
                    var dateTime = (DateTime) dataReader["Time"];
                    runs.Add(new RunSummary(dateTime, runId));
                }

                return runs;
            }
        }
    }
}