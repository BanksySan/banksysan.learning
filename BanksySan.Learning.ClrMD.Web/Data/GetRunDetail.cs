﻿namespace BanksySan.Learning.ClrMD.Web.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Models;

    public class GetRunDetail
    {
        public RunInfo Execute(Guid runId)
        {
            using (var connection =
                new SqlConnection("Data Source=localhost;Initial Catalog=Telemetry;Integrated Security=True"))
            {
                connection.Open();
                DateTime runTime;


                var probeDetails = new List<(Guid Id, DateTime Time)>();

                using (var probeDetailsCommand = ProbeDetailsCommand(runId, connection))
                {
                    using (var probeDetailsReader = probeDetailsCommand.ExecuteReader())
                    {
                        while (probeDetailsReader.Read())
                        {
                            var id = (Guid) probeDetailsReader["Id"];
                            var time = (DateTime) probeDetailsReader["Time"];

                            probeDetails.Add((id, time));
                        }
                    }
                }

                var probes = new List<ProbeInfo>();

                foreach (var probeDetail in probeDetails)
                {
                    var segmentInfos = new List<SegmentInfo>();

                    using (var segmentsDetailsCommand = SegmentsDetailsCommand(probeDetail.Id, connection))
                    {
                        using (var segmentDetailsReader = segmentsDetailsCommand.ExecuteReader())
                        {
                            while (segmentDetailsReader.Read())
                            {
                                var start = BitConverter.ToUInt64((byte[]) segmentDetailsReader["StartAddress"],0);
                                var size = BitConverter.ToUInt64((byte[]) segmentDetailsReader["Size"], 0);
                                var commitedEnd = BitConverter.ToUInt64((byte[]) segmentDetailsReader["CommitedEnd"], 0);
                                var ephemaral = (bool) segmentDetailsReader["Ephemaral"];
                                var large = (bool) segmentDetailsReader["Large"];
                                var reservedEnd = BitConverter.ToUInt64((byte[]) segmentDetailsReader["ReservedEnd"], 0);

                                segmentInfos.Add(new SegmentInfo(start, size, commitedEnd, ephemaral, large,
                                                                 reservedEnd));
                            }
                        }
                    }

                    probes.Add(new ProbeInfo(segmentInfos, probeDetail.Time));
                }


                using (var runIdDetailsCommand = RunDetailsCommand(runId, connection))
                {
                    runTime = (DateTime) runIdDetailsCommand.ExecuteScalar();
                }

                return new RunInfo(probes, runTime);
            }
        }

        private SqlCommand ProbeDetailsCommand(Guid id, SqlConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandText = "GetProbeDetails";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            var runIdParameter = command.CreateParameter();
            runIdParameter.DbType = DbType.Guid;
            runIdParameter.Value = id;
            runIdParameter.ParameterName = "@RunId";
            command.Parameters.Add(runIdParameter);
            return command;
        }

        private SqlCommand RunDetailsCommand(Guid id, SqlConnection connection)
        {
            var command = connection.CreateCommand();
            command.CommandText = "GetRunDetails";
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = connection;

            var runIdParameter = command.CreateParameter();
            runIdParameter.DbType = DbType.Guid;
            runIdParameter.Value = id;
            runIdParameter.ParameterName = "@RunId";
            command.Parameters.Add(runIdParameter);
            return command;
        }

        private static SqlCommand SegmentsDetailsCommand(Guid probeId, SqlConnection connection)
        {
            var segmentsDetailsCommand = connection.CreateCommand();
            segmentsDetailsCommand.CommandText = "GetSegmentDetails";
            segmentsDetailsCommand.CommandType = CommandType.StoredProcedure;
            var probeIdParameter = segmentsDetailsCommand.CreateParameter();
            probeIdParameter.DbType = DbType.Guid;
            probeIdParameter.Value = probeId;
            probeIdParameter.ParameterName = "@ProbeId";
            segmentsDetailsCommand.Parameters.Add(probeIdParameter);
            return segmentsDetailsCommand;
        }
    }
}