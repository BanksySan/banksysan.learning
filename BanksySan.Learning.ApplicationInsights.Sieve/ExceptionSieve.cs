﻿// ReSharper disable PossibleIntendedRethrow
namespace BanksySan.Learning.ApplicationInsights.Sieve
{
    using System;

    public class ExceptionSieve
    {
        public TException RemoveStack<TException>(TException exception) where TException : Exception
        {
            try
            {
                try
                {
                    throw exception;
                }
                catch (TException e)
                {
                    throw e;
                }
            }
            catch (TException e)
            {
                return e;
            }
        }
    }
}