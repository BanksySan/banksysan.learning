﻿namespace BanksySan.Learning.Scratch.dotNetCore
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using static System.Console;

    internal static class Program
    {
        private static void Main(string[] args)
        {
            WindowWidth = 100;
            BufferWidth = 500;
            Title = $"Scratch: {Process.GetCurrentProcess().Id}";

            using (var connection =
                new SqlConnection(@"Data Source=plod;Initial Catalog=Scratch;Integrated Security=True"))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText =
                        @"SELECT [Id],[Description] FROM [dbo].[Table1]; SELECT [Id],[Description] FROM [dbo].[Table2];";
                    command.CommandType = CommandType.Text;
                    connection.Open();

                    const int WIDTH = 15;

                    using (var dataReader = command.ExecuteReader())
                    {
                        do
                        {
                            WriteLine($"|{dataReader.GetName(0),WIDTH}|{dataReader.GetName(1),WIDTH}|");
                            WriteLine($"|{new string('-', WIDTH)}|{new string('-', WIDTH)}|");
                            var columnCount = dataReader.FieldCount;

                            while (dataReader.Read())
                            {
                                for (var i = 0; i < columnCount; i++)
                                {
                                    Write($"|{dataReader[i],WIDTH}");
                                }
                                WriteLine("|");
                            }
                        } while (dataReader.NextResult());
                    }
                }
            }
        }
    }
}