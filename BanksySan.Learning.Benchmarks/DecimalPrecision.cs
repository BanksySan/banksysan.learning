namespace BanksySan.Learning.Benchmarks
{
    using System;
    using System.Text.RegularExpressions;
    using BenchmarkDotNet.Attributes;

    public class DecimalPrecision
    {
        private static readonly Random RANDOM = new Random();

        [Benchmark(Baseline = true)]
        public bool CheckPrecisionRegex()
        {
            var d = new decimal(RANDOM.NextDouble());
            var precision = RANDOM.Next(5);

            var regex = new Regex(@"^\d*\.(?<decimals>\d*)");
            var decimalString = d.ToString();

            var matches = regex.Match(decimalString);
            var decimals = matches.Groups["decimals"];
            return decimals.Length <= precision;
        }

        [Benchmark]
        public bool CheckPrecisionBitConverter()
        {
            var d = new decimal(RANDOM.NextDouble());
            var precision = RANDOM.Next(5);

            return BitConverter.GetBytes(decimal.GetBits(d)[3])[2] <= precision;
        }

        [Benchmark]
        public static bool CheckPrecisionModulo()
        {
            var d = new decimal(RANDOM.NextDouble());
            var precision = RANDOM.Next(5);
            var modulus = 1m / (decimal) Math.Pow(10, precision);

            return d % modulus == 0;
        }
    }
}