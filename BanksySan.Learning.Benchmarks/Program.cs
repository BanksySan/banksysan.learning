﻿namespace BanksySan.Learning.Benchmarks
{
    using System;
    using BenchmarkDotNet.Running;

    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.BufferWidth = 200;
            Console.WindowWidth = Console.BufferWidth;

            var summary = BenchmarkRunner.Run<DecimalPrecision>();
            Console.WriteLine("Done.");
            Console.Read();
        }
    }
}