namespace BanksySan.Learning.DependencyGraph.Application
{
    using System;
    using System.Collections.Generic;

    class Node : INode
    {
        private readonly List<INode> _dependencies = new List<INode>();

        public bool AddDependency(Node node)
        {
            try
            {
                _dependencies.Add(node);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        public IEnumerable<INode> Dependencies => _dependencies;
    }
}