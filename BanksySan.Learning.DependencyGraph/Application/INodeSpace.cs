﻿namespace BanksySan.Learning.DependencyGraph.Application
{
    using System.Collections.Generic;

    internal interface INodeSpace : IEnumerable<INode>
    {
        void Add(INode node);
    }
}