﻿namespace BanksySan.Learning.DependencyGraph.Application
{
    using System.Collections.Generic;

    internal interface INode
    {
        IEnumerable<INode> Dependencies { get; }
    }
}