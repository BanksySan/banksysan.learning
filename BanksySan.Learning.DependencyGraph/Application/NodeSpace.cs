﻿namespace BanksySan.Learning.DependencyGraph.Application
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    internal class NodeSpace : INodeSpace
    {
        private readonly IList<INode> _nodes = new List<INode>();

        public NodeSpace(Node[] nodes)
        {
            foreach (var node in nodes)
            {
                _nodes.Add(node);
            }
        }

        public IEnumerable<INode> Roots => _nodes.Where(x => !x.Dependencies.Any());

        public void Add(INode node)
        {
            _nodes.Add(node);
        }

        public IEnumerator<INode> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}