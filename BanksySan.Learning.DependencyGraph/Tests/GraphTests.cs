﻿namespace BanksySan.Learning.DependencyGraph.Tests
{
    using System;
    using static System.Console;
    using System.Collections.Generic;
    using System.Linq;
    using Application;
    using NUnit.Framework;

    [TestFixture]
    public class GraphTests
    {
        [Test]
        public void BuildOrder()
        {
            var node1 = new Node();
            var node2 = new Node();
            var node3 = new Node();
            var node4 = new Node();
            var node5 = new Node();
            var node6 = new Node();

            node1.AddDependency(node2);
            node1.AddDependency(node3);
            node2.AddDependency(node3);
            node2.AddDependency(node4);
            node3.AddDependency(node5);
            node2.AddDependency(node6);

            var nodeSpace = new NodeSpace(new[] {node1, node2, node3, node4, node5, node6});

            var visited = new HashSet<INode>();
            var unvisited = new HashSet<INode>(nodeSpace);
            const string VISITED = "Visited";
            const string UNVISITED = "Unvisited";
            const string SKIP = "Skiped";
            const string PROCESS = "Process";

            WriteLine($"{nameof(node1)} {node1.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node1.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");
            WriteLine($"{nameof(node2)} {node2.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node2.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");
            WriteLine($"{nameof(node3)} {node3.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node3.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");
            WriteLine($"{nameof(node4)} {node4.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node4.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");
            WriteLine($"{nameof(node5)} {node5.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node5.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");
            WriteLine($"{nameof(node6)} {node6.GetHashCode():X}");
            WriteLine($"|-> {string.Join(", ", node6.Dependencies.Select(x => x.GetHashCode().ToString("X")))}");

            while (unvisited.Any())
            {
                var newStack = unvisited.ToArray();
                WriteLine();
                for (var i = 0; i < newStack.Length; i++)
                {
                    var node = newStack[i];

                    Write($"{i,-5}|{node.GetHashCode(),10:X}|");
                    if (visited.Contains(node))
                    {
                        Write($"{VISITED,13}|{SKIP, 8}|");
                    }
                    else
                    {
                        var dependencies = node.Dependencies;
                        Write($"{UNVISITED,13}|");

                        if (dependencies.Intersect(visited).SequenceEqual(dependencies))
                        {
                            Write($"{PROCESS,10}|");
                            unvisited.Remove(node);
                            visited.Add(node);
                        }
                        else
                        {
                            Write($"{SKIP, 10}|");
                        }
                    }
                    WriteLine();
                }
            }
        }
    }
}