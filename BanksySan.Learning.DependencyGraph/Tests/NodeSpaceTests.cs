﻿namespace BanksySan.Learning.DependencyGraph.Tests
{
    using System.Linq;
    using Application;
    using NUnit.Framework;

    [TestFixture]
    public class NodeSpaceTests
    {
        [Test]
        public void AllNodesRetrieved()
        {
            var node1 = new Node();
            var node2 = new Node();

            var nodes = new[] {node1, node2};
            var nodeSpace = new NodeSpace(nodes);

            CollectionAssert.AreEquivalent(nodeSpace, nodes);
            CollectionAssert.AreEquivalent(nodeSpace, nodes.Reverse());
            CollectionAssert.AreNotEquivalent(nodeSpace, new[] {node1, node2, new Node()});
        }

        [Test]
        public void FindRoot()
        {
            var node1 = new Node();
            var node2 = new Node();
            var node3 = new Node();
            var node4 = new Node();
            var node5 = new Node();

            node1.AddDependency(node2);
            node2.AddDependency(node3);
            node3.AddDependency(node4);
            node4.AddDependency(node5);

            var nodeSpace = new NodeSpace(new [] {node1, node2, node3, node4, node5});

            var roots = nodeSpace.Roots;

            Assert.That(roots, Is.EquivalentTo(new[] {node5}));
        }

        [Test]
        public void FindMultipleRoots()
        {
            var node1 = new Node();
            var node2 = new Node();
            var node3 = new Node();
            var node4 = new Node();
            var node5 = new Node();

            node1.AddDependency(node2);
            node1.AddDependency(node2);
            node3.AddDependency(node4);
            node3.AddDependency(node4);

            var nodeSpace = new NodeSpace(new[] { node1, node2, node3, node4, node5 });

            var roots = nodeSpace.Roots;

            Assert.That(roots, Is.EquivalentTo(new[] { node2, node4, node5 }));
        }
    }
}