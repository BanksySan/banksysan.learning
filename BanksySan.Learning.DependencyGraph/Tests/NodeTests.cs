﻿namespace BanksySan.Learning.DependencyGraph.Tests
{
    using Application;
    using NUnit.Framework;

    [TestFixture]
    public class NodeTests
    {
        [Test]
        public void DependenciesRetrieved()
        {
            var node1 = new Node();
            var node2 = new Node();
            var node3 = new Node();

            node1.AddDependency(node2);
            node1.AddDependency(node3);
            Assert.That(node1.Dependencies, Is.EquivalentTo(new[] {node2, node3}));
            Assert.That(node1.Dependencies, Is.EquivalentTo(new[] {node3, node2}));
        }
    }
}